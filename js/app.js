document.addEventListener('DOMContentLoaded', function() {

    const mobilePage = document.querySelector('.mobile-page'),
          container = document.querySelector('.slides-container');

    let mediaQueryList = window.matchMedia('(max-width: 1024px)'),
        changeToMobile = function() {
            if (mediaQueryList.matches) {
                mobilePage.classList.remove('no-display');
                container.classList.add('no-display');
            } else {
                container.classList.remove('no-display');
                mobilePage.classList.add('no-display');
            }
        };

    changeToMobile();
    window.addEventListener('resize', changeToMobile);

    let mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });

});